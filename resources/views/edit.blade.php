@extends('layouts.app')
@section('content')
    <h1>แก้ไขข้อมูล</h1>
    <form method="post" action="{{ url('/todo/'.$todo->id) }}">
        @csrf
        @method('PUT')
        <div class="form-group">
            <label>ประเภทภาพถ่าย</label>
            <input type="text" name="title" class="form-control" value="{{ $todo->title }}">
        </div>
        <div class="form-group">
            <label>งบประมาณ</label>
            <input type="text" name="content" class="form-control" value="{{ $todo->content }}">
        </div>
        <div class="form-group">
            <label>วันที่</label>
            <input type="date" name="due" class="form-control" value="{{ $todo->due }}">
        </div>
        <button type="submit">Confirm</button>
    </form>
@endsection

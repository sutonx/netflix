@extends('layouts.app')
@section('content')<br>
    <a href="{{ url('/todo/'.$todo->id.'/edit') }}" class="btn btn-primary">Edit</a>
  <div align="right">  <form action=" {{ url('/todo/'.$todo->id) }}" method="post" id="form-delete">
    @method('DELETE')
    @csrf
    <button class="btn btn-danger" onclick="confirm_delete()" type="button">Delete</button>
    </form>
  </div>
<h1>{{$todo->title}}</h1>
    <p>{{$todo->due}}</p>
    <hr>
    <p>{{$todo->content}}</p>

    <a href="{{ url('/') }}" class="btn btn-secondary">Back</a>
    <script>
        function confirm_delete() {
            var text = '{!! $todo->title !!}';
        var confirm = window.confirm('นั่งยันการลบ' + text);
        if (confirm){
            document.getElementById('form-delete').submit();
        }

        }
    </script>
@endsection
